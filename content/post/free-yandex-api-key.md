---
title: Get a free Yandex API key
date: 2018-08-26
---

Fedilab uses yandex API for translations because it allows for free a daily request limit of 1,000,000 characters and a monthly limit of 10,000,000 characters. While, it seems to be enough, Mastalab shares the API key with all users, that means one day or another, the shared key might reach one of these limits.

The good news, Fedilab allows to set API keys, so that you can create your own API key, shared with no-one else.

This post will explain you how to easily create your own API key using Yandex Translate.

First, you need to navigate to this page: [https://tech.yandex.com/translate/](https://tech.yandex.com/translate/)

The first point, will tell you that you can test translations, but that’s not really important here.

The second point, will present you the user agreement and rules for formatting translation results. The app does respect these points (if you notice any changes, please report them to me).

The third point will allow to create the key that you will insert in the application.

1 – So click on “Get a free API key“. You will need to create your account or use an existing one.

2 – At the top right, click on “+ Create a new key”

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/free-yandex-api-key/01.png" caption="Create a new key">}}
{{< /gallery >}}

3 – Write a small description for this key (it will be useful, if you manage several keys)

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/free-yandex-api-key/02.png" caption="Key description">}}
{{< /gallery >}}

4 – Then, you will get your key. You can notice, that is possible to deactivate the key (I will deactivate this key after tests because I displayed it).

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/free-yandex-api-key/03.png" caption="New API key">}}
{{< /gallery >}}


5 – Next step, is to copy and paste this key into Fedilab settings. If you created this key on a computer, you can send it to your phone by email. Then open settings from the left menu. Go in the first tab and scroll to translation settings. Just copy/paste your key.

{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/free-yandex-api-key/04.png" caption="Open settings">}}
 {{< figure link="../img/free-yandex-api-key/05.png" caption="Long press the key field">}}
  {{< figure link="../img/free-yandex-api-key/06.png" caption="Paste the API key">}}
{{< /gallery >}}


Fedilab uses MyTransL a library that I have developed. This library is set in Fedilab to obfuscate sensitive data before sending them to a translation API:
{{< gallery caption-effect="fade" >}}
  {{< figure link="../img/free-yandex-api-key/07.png" caption="Screenshot of MytransL">}}
{{< /gallery >}}

Every emails / mentions / tags / URLs will be obfuscated before being sent to the API.

#### Article from [@apps](https://toot.fedilab.app/@apps)