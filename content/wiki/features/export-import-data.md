*[Home](../../home) > [Features](../../features) &gt;* Export/Import data

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Export/Import Data
This feature allows you to export data of Fedilab app.


The exported data will be saved in the root directory of your devices internal storage. The name of the file will be like this:
<br>
<br>
**Fedilab&#95;export&#95;&lt;date&gt;&#95;&lt;time&gt;.fedilab**
<br>
<br>
You can use this file to import previously exported data into Fedilab. You also can open this file on other platforms with a SQLite database viewer (e.g. <a href="https://sqlitebrowser.org/" target="_blank" rel="noopener noreferrer">DB Browser for SQLite</a>).

### How to export data
1. Open the navigation drawer and press the three-dot-menu on the top. <br><br> <img src="../../res/export_import/nav.png" width="300px">

2. Select 'Export data' option. <br><br> <img src="../../res/export_import/menu_export.png" width="300px">

<br>
<br>

### How to import data
1. Open the navigation drawer and press the three-dot-menu on the top. <br><br> <img src="../../res/export_import/nav.png" width="300px">

2. Select 'Import data' option. <br><br> <img src="../../res/export_import/menu_import.png" width="300px">

3. Now choose a previously exported data file from your device's storage.<br>*If you import previously exported data to a new installation of Fedilab, it will log you out of your account. Login to your account again and you would be able to see imported data.*

#### What data is included
Here's some of the things which will be included in the exported file

  - **Bookmarks**

  - **Scheduled toots** <br> *Includes your scheduled toot and scheduled boosts, This* ***doesn't include*** *toots scheduled using the* ***server side method***

  - **Information in your profile page** <br> *Includes: username, display name, locked status, number of followers, number of followings, number of statuses, profile description, account url, url of the profile picture, url of the header image, instance type (mastodon, peertube), instance url, oauth token and creation time and some other information*

  - **Followed instances** <br> *List of instances you have followed using the 'follow instance' button in Fedilab*

  - **Favorited peertube videos**

  - **Locally stored toots** <br> *If you have used 'Your toots' feature in Fedilab to locally store your toots, they will be included*

  - **Temporarily muted accounts** <br> *A list of accounts muted temporarily using 'Timed mute' option*
