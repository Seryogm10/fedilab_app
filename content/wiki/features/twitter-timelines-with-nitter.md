*[Home](../../home) > [Features](../../features) &gt;* Follow Twitter timelines with Nitter

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Follow Twitter timelines with Nitter

Since release `2.27.1` the app allows to follow Twitter timelines by using [Nitter](https://github.com/zedeus/nitter) which is a free and open source alternative Twitter front-end focused on privacy.

The goal of this integration is to allow you to follow several Twitter feeds from different accounts in a same timeline. Twitter servers are never used, and you will never interact directly with these Twitter accounts.

Following these accounts will use the [RSS feeds](https://github.com/zedeus/nitter/issues/96) from a Nitter instance that works with pagination.

#### How to add a timeline

1- First go in **Reorder timelines** from the main menu.

<img src="../../res/twitter-timelines-with-nitter/reorder_timelines.png" alt="Reorder timelines from the main menu" width=300/>

2- Then click on the top right **"+"** icon to add a followed instance.

<img src="../../res/twitter-timelines-with-nitter/add_followed_instance.png" alt="Add a followed instance" width=300/>


3- At the bottom of the radio buttons, tip on **Twitter accounts (via Nitter)**

<img src="../../res/twitter-timelines-with-nitter/add_nitter_accounts.png" alt="Select Twitter accounts (via Nitter)" width=300/>

4- Add several Twitter accounts by writing their username separated with spaces

<img src="../../res/twitter-timelines-with-nitter/add_several_nitter_accounts.png" alt="Write several Twitter usernames separated with spaces" width=300/>

Once validated you will see a new item in your timelines:

<img src="../../res/twitter-timelines-with-nitter/nitter_followed_accounts_item.png" alt="Item showing all followed accounts in a same timeline" width=300/>


5- The Nitter timeline

Now you have your custom timeline where you can share messages to your community by respecting their privacy.

<img src="../../res/twitter-timelines-with-nitter/nitter_timeline.png" alt="The Nitter timeline" width=300/>


----

Do not hesitate to have a look to [Reorder timelines](../reorder-timelines). Nitter timelines are managed like followed instances.
