*[Home](../../home) > [Features](../../features) &gt;* Push notifications

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Push notifications

Since version 2.38.0, Fedilab now works with [push notifications](https://en.wikipedia.org/wiki/Push_technology#Webpush). Live and delayed notifications have been removed.
For working, it needs a provider that will deliver notifications to the app. This will allow multiple apps to share the same network connection, rather than each app using its own connection, saving overall battery. You are up to install your own [push distributor](https://unifiedpush.org/users/distributors/), but the following is one example of how to make it work.

PS: This step by step is more dedicated to F-Droid users. If you use the Google Play version, this is not needed. But you can follow this guide to change your provider (by default it is Firebase).

This work has been done thanks to the help of [UnfiedPush](https://unifiedpush.org/). 


### Install the provider

First you need to install the provider. The easiest is to use `ntfy`.

* Install ntfy : <https://f-droid.org/packages/io.heckel.ntfy> and open it.
* Restart fedilab

That's done!

### More

If you want to use another distributor or self host your server, visit <https://unifiedpush.org>.

Now Fedilab will automatically uses ntfy for pushing notifications.


